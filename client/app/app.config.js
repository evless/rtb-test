function appConfig($routeProvider, $locationProvider) {
    $locationProvider.html5Mode(true);

    $routeProvider
        .when('/', {
            template: require('../templates/dialog.html')
        })
        .otherwise('/')
}

appConfig.$inject = ['$routeProvider', '$locationProvider']

export default appConfig;