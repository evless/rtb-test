class appService {
    constructor($http) {
        this._http = $http;
    }

    getRandomUser() {
        return this._http.get('https://randomuser.me/api/');
    }
}

appService.$inject = ['$http'];

export default appService;