import emailsEditorController from './emailsEditor.controller';

import './emailsEditor.css';

export default {
    template: require('./emailsEditor.html'),
    controller: emailsEditorController,
    bindings: {
        emails: '<'
    }
}