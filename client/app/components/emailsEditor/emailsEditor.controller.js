import angular from 'angular';
import EmailConstructor from '../../constructors/email';
import { SPLIT_EMAILS, REPLACEMENT_EMAIL } from '../../constans/email';

const classes = {
    parent: '.emails-editor',
    container: '.emails-editor__container',
    item: '.emails-editor__item',
    itemSelected: '.emails-editor__item_select',
    itemText: '.emails-editor__text',
    textArea: '.emails-editor__textarea'
};

class EmailsEditorController extends EmailConstructor {
    constructor($element, $timeout, $window) {
        super();
        this.timeout = $timeout;

        this.textAreaWidth = '100%'; // Ширина поля
        this.selectedEmailIndex = null; // Индекс выбранной почты
        this._newEmail = null; // Модель текстового поля

        this.element = $element[0].querySelector(classes.parent);
        this.container = this.element.querySelector(classes.container);
        this.textArea = this.element.querySelector(classes.textArea);
        
        this.$onInit = () => {
            $window.addEventListener('resize', this.windowResize.bind(this));
            $window.addEventListener('keydown', this.windowKeyDown.bind(this));
        }

        this.$onDestroy = () => {
            $window.removeEventListener('resize', this.windowResize.bind(this));
            $window.removeEventListener('keydown', this.windowKeyDown.bind(this));
        }

        // При добавлении писем в массив из внешней среды, нужно пересчитывать ширину поля
        this.prevEmails;
        this.$doCheck = () => {
            if (this.emails) {
                if (!angular.equals(this.emails, this.prevEmails)) {
                    this.prevEmails = this.emails.slice(0, this.emails.length);
                    this.calculateTextareaWidth();
                }
            }
        }

        this.calculateWidth();
    }

    calculateWidth() {
        this.width = this.element.offsetWidth; // Ширина всего блока
        this.minTextAreaWidth = this.width / 2; // Минмальная ширина текстового поля
    }

    /**
     * При ресайзе окна будет пересчитываться размер текстового поля.
     */
    windowResize() {
        this.calculateWidth();
        this.calculateTextareaWidth();
    }

    /**
     * Прослушка нажатий кнопок ArrowLeft, ArrowRight
     * Для перемещения между выбранным адресом или удалением
     * 
     * Backspace для удаления выбранного адреса
     * 
     * Enter для создания адреса
     * 
     * @param {Object} event -> Эвент нажатия кнопки
     */
    windowKeyDown(event) {
        switch (event.key) {
            case 'ArrowLeft':
                if (null !== this.selectedEmailIndex) {
                    if (this.selectedEmailIndex > 0) {
                        this.timeout(this.onSelectEmail.bind(this, --this.selectedEmailIndex), 0)
                    }
                }
                break;
            
            case 'ArrowRight':
                if (null !== this.selectedEmailIndex) {
                    if (this.selectedEmailIndex < this.emails.length - 1) {
                        this.timeout(this.onSelectEmail.bind(this, ++this.selectedEmailIndex), 0)
                    }
                }
                break;

            case 'Backspace':
                let selectElement = this.element.querySelector(`${classes.itemSelected} ${classes.itemText}`);
                if (selectElement) {
                    this.removeEmail(this.findEmailIndex(selectElement.textContent))
                    return false;
                }
                break;

            case 'Enter':
                event.preventDefault();
                this.timeout(() => {
                    this.regroupEmails(this._newEmail);
                }, 0);
                break;

            default:
                break;
        }
    }

    /**
     * Сеттер/Геттер модели в поле ввода.
     * Если произошла вставка или в поле как-то появилось неожиданно много символов, то делаем добавление.
     */
    set newEmail(data) {
        if (null !== data || undefined !== data) {
            // Проверяем если до этого не было ничего в модели и появилось больше 1 символа
            // Или если модель была, то изменилась больше чем на 1 символ
            if ((!this._newEmail && 1 < data.length) || (this._newEmail && this._newEmail.length < data.length - 1)) {
                this.regroupEmails(data);
                this._newEmail = null;
                return;
            }

            this._newEmail = data;
        }
    }

    get newEmail() {
        return this._newEmail;
    }

    /**
     * Сплит строки и отдельное добавление каждого элемента.
     */
    regroupEmails(emails) {
        if (emails) {
            let result = emails.split(SPLIT_EMAILS);
            result.map(this.addEmail.bind(this));
        }
    }

    /**
     * Добавление элемента.
     * Если в директиву не прокинули модель или её типа был не Array, то выводим ошибку.
     * Если такой адрес уже есть, то выводим alert.
     * 
     * @param {String} email -> Добавляемый адрес
     */
    addEmail(email) {
        if (email) {
            let emailReplacement = email.replace(REPLACEMENT_EMAIL, '');
            
            // Вдруг кто-то передал null или что-то отличное от массива
            if (!this.emails && !Array.isArray(this.emails)) {
                throw new Error('Вы не передали emails, или передан не массив.')
            }
            
            if (!this.checkEmail(emailReplacement)) {
                this.emails.push(emailReplacement);
            } else {
                alert(`Такой E-mail уже добавлен: ${emailReplacement}`);
            }

            this._newEmail = null;
        }
    }

    /**
     * Обработчик на изменения текстового поля.
     */
    newEmailChange() {
        if (this._newEmail) {
            let str = this._newEmail;
            let lastSymbol = str.substr(str.length - 1, str.length);
            let reg = REPLACEMENT_EMAIL;

            // Проверка, если был добавлен только 1 символ и проверка нужно при написании этого символ
            // Добавлять адрес
            if (1 === lastSymbol.length && reg.test(lastSymbol)) {
                this.regroupEmails(this._newEmail);
            }
        }
    }

    /**
     * Метод рассчета ширины текстового поля. В зависимости от кол-ва адресов и ширины родительского блока.
     * 
     * @param {Function} callback -> Фкнция, которая вызывается после перерасчета ширины текстового поля
     */
    calculateTextareaWidth(callback) {
        this.timeout(() => {
            let items = this.container.querySelectorAll(classes.item);
            let clearWidth = 0;

            Object.keys(items)
                .map(item => items[item].offsetWidth)
                .map(item => {
                    if (this.width - (clearWidth + item) >= this.minTextAreaWidth) {
                        clearWidth += item;
                    } else {
                        clearWidth = 0;
                    }
                });

            let summaryWidth = this.width - clearWidth;
            let width;
            if (summaryWidth > this.minTextAreaWidth) {
                width = summaryWidth - 40; 
            } else {
                width = this.width;
            }
            
            this.textAreaWidth = `${width}px`;
            
            if ('function' === typeof callback) {
                this.timeout(callback.bind(this), 0);
            }
        }, 0);
    }

    /**
     * Удаление адреса и вызов перерассчета шарины текстового блока
     * 
     * @param {Number} index -> Индекс удаляемого элемента в массиве
     */
    removeEmail(index) {
        if (this.emails) {
            this.emails.splice(index, 1);
            this.calculateTextareaWidth();
            this.selectedEmailIndex = null;
        }
    }

    /**
     * Отмечает выбранным конкретный адрес. Выборка происходит по индексу.
     * 
     * @param {Number} index -> Номер выбираемого адреса в массиве
     */
    onSelectEmail(index) {
        this.selectedEmailIndex = index;
    }

    /**
     * Обработчик при вводе в поле ввода
     * 
     * @param {Object} event -> Эвент нажатия кнопки
     */
    keyDownHandler(event) {
        if ('Backspace' === event.key && (!this._newEmail || '' === this._newEmail) && 0 < this.emails.length) {
            this.removeEmail(this.emails.length - 1);
        }
    }
}

EmailsEditorController.$inject = ['$element', '$timeout', '$window'];

export default EmailsEditorController;