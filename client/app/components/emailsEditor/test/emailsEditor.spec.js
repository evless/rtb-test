const classes = {
    item: '.emails-editor__item',
    itemSelected: '.emails-editor__item_select',
    itemText: '.emails-editor__text',
    invalid: '.emails-editor__item_type_invalid',
    cross: '.emails-editor__cross'
};

describe('EmailEditor', () => {
    let scope, $compile, element;
    beforeEach(module('components'));

    beforeEach(inject((_$compile_, _$rootScope_) => {
        // The injector unwraps the underscores (_) from around the parameter names when matching
        $compile = _$compile_;
        scope = _$rootScope_.$new();
        element = angular.element('<emails-editor emails="emails"></emails-editor>');

        $compile(element)(scope);
    }));

    it('Проверка подключения директивы', () => {
        expect(element.html()).toContain('class="emails-editor"');
    });

    it('Проверка прокидывания массива', () => {
        scope.emails = ['aa@aa.ru', 'bb@bb.com'];
        scope.$digest();
        
        let items = element[0].querySelectorAll(classes.item);
        expect(items.length).toBe(2);

        let firstItemText = items[0].querySelector(classes.itemText);
        expect(firstItemText.textContent).toContain('aa@aa.ru');

        let secondItemText = items[1].querySelector(classes.itemText);
        expect(secondItemText.textContent).toContain('bb@bb.com');
    });

    it('Проверка валидности почтовых ящиков', () => {
        scope.emails = ['valid@mail.ru', 'invalid'];
        scope.$digest();
        
        let items = element[0].querySelectorAll(classes.item);
        expect(items.length).toBe(2);

        let firstItem = items[0];
        expect(firstItem.className).not.toContain(classes.invalid.substr(1));

        let secondItem = items[1];
        expect(secondItem.className).toContain(classes.invalid.substr(1));
    });

    it('Удаление емейли через крестик', () => {
        scope.emails = ['valid@mail.ru', 'invalid'];
        scope.$digest();
        
        let items = element[0].querySelectorAll(classes.item);

        let firstElementCross = items[0].querySelector(classes.cross);
        firstElementCross.click();
        scope.$digest();

        expect(scope.emails[0]).toContain('invalid');
        expect(scope.emails.length).toBe(1);
    });

    it('Проверка добавление валидного адреса через поле ввода', () => {
        scope.emails = [];
        scope.$digest();

        let textarea = element.find('textarea');
        let ngModelCtrl = textarea.controller('ngModel')
        ngModelCtrl.$setViewValue('lala@lala.ru');
        scope.$digest();

        expect(scope.emails.length).toBe(1);

        let items = element[0].querySelectorAll(classes.item);
        expect(items.length).toBe(1);

        expect(items[0]).not.toContain(classes.invalid.substr(1));
    });

    it('Проверка добавление нескольких адресов через поле ввода', () => {
        scope.emails = [];
        scope.$digest();

        let textarea = element.find('textarea');
        let ngModelCtrl = textarea.controller('ngModel')
        ngModelCtrl.$setViewValue('lala@lala.ru; blabla@blabla.ru ggggg');
        scope.$digest();

        expect(scope.emails.length).toBe(3);
        
        let items = element[0].querySelectorAll(classes.item);
        expect(items.length).toBe(3);

        let firstItem = items[0];
        expect(firstItem.className).not.toContain(classes.invalid.substr(1));

        let secondItem = items[1];
        expect(secondItem.className).not.toContain(classes.invalid.substr(1));

        let thirdItem = items[2];
        expect(thirdItem.className).toContain(classes.invalid.substr(1));
    })
});