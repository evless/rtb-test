import angular from 'angular';
import emailsEditor from './emailsEditor/emailsEditor';

let componentsModule = angular.module('components', []);
componentsModule.component('emailsEditor', emailsEditor);

export default componentsModule.name;