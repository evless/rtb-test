import angular from 'angular';
import ngRoute   from 'angular-route';
import components from './components/components';
import appController from './app.controller';
import appConfig from './app.config';
import appService from './app.service';

import '../public/css/index.css';

angular
    .module('app', [components, 'ngRoute'])
    .config(appConfig)
    .controller('appController', appController)
    .service('appService', appService);