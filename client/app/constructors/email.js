import { VALIDATION_EMAIL } from '../constans/email';


/**
 * Конструктор для работы с массивом адресов
 * - Проверка существования адресса в массиве
 * - Поиск индекса адреса в массиве
 * - Валидация адреса
 */
export default class EmailConstructor {
    constructor() {
        this.emails = [];
    }

    /**
     * Поиск конкретного адреса в списке
     * 
     * @param  {String} email -> Искомый адрес
     * @return {Boolean}
     */
    checkEmail(email) {
        return this.emails.find(item => item === email);
    }

    /**
     * Поиск индекса конктретного адреса
     * 
     * @param  {String} email -> Искомый адрес
     * @return {Number}
     */
    findEmailIndex(email) {
        return this.emails.findIndex(item => item === email);
    }

    /**
     * Валидация адреса
     * 
     * @param  {String} email -> Валедируемый адрес
     * @return {Boolean}
     */
    validEmail(email) {
        return VALIDATION_EMAIL.test(email);
    }
}