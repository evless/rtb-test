class appController {
    constructor($http, $timeout, appService) {
        this.appService = appService;
        this._timeout = $timeout;
        this._http = $http;
        this.emails = [];
        this.disabled = false;
    }

    /**
     * Добавление нового адреса через API
     */
    addRandomEmail() {
        this.disabled = true;
        this.appService.getRandomUser()
            .then(response => {
                // Предотвращаем быстрое нажимание кнопки "Add email"
                this._timeout(() => {
                    this.disabled = false
                }, 100);
                
                // Проверка, вдруг изначально что-то было отличное от массива
                if (!this.emails) {
                    this.emails = [];
                }

                this.emails.push(response.data.results[0].email);
            })
            .catch(err => console.error(err));
    }

    /**
     * Показываем кол-во адресов в массиве
     */
    showEmailsCount() {
        alert(`Emails count: ${this.emails.length}!`);
    }
}

appController.$inject = ['$http', '$timeout', 'appService'];

export default appController;